package com.project.mva.xmunofficialcatalogue.local;

import android.app.Activity;
import android.content.SharedPreferences;

/**
 * Created by martinInExt on 25/04/2017.
 */

public class LocalPref extends Activity{

    public static final String PREF_NAME = "PAKMVA01";

    public static final String ACTIVEUNIVERSE = "PAKMVAactiveuniverse";
    public static final String ACTIVEUNIVERSE_VALUE = "Square-Enix";

    public String getActiveUniverseValue(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        return settings.getString(ACTIVEUNIVERSE, ACTIVEUNIVERSE_VALUE);
    }

    public boolean updateActiveUniverseValue(Activity activity, String activeUniverseValue) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(ACTIVEUNIVERSE, activeUniverseValue);

        return editor.commit();
    }

    public static final String NAME = "PAKMVAname";
    public static final String NAME_VALUE = "Play Arts Kai";

    public String getNameValue(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        return settings.getString(NAME, NAME_VALUE);
    }

    public boolean updateNameValue(Activity activity, String nameValue) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(NAME, nameValue);

        return editor.commit();
    }

    public static final String UNIVERSE = "PAKMVAuniverse";
    public static final String UNIVERSE_VALUE = "Square Enix";

    public String getUniverseValue(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        return settings.getString(UNIVERSE, UNIVERSE_VALUE);
    }

    public boolean updateUniverseValue(Activity activity, String universeValue) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(UNIVERSE, universeValue);

        return editor.commit();
    }

    public static final String OGPRICE = "PAKMVAogprice";
    public static final String OGPRICE_VALUE = "$1.00";

    public String getOgpriceValue(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        return settings.getString(OGPRICE, OGPRICE_VALUE);
    }

    public boolean updateOgpriceValue(Activity activity, String ogpriceValue) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(OGPRICE, ogpriceValue);

        return editor.commit();
    }

    public static final String RELEASEDATE = "PAKMVAreleasedate";
    public static final String RELEASEDATE_VALUE = "01/01/2008";

    public String getReleaseDateValue(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        return settings.getString(RELEASEDATE, RELEASEDATE_VALUE);
    }

    public boolean updateReleaseDateValue(Activity activity, String releasedateValue) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(RELEASEDATE, releasedateValue);

        return editor.commit();
    }

    public static final String DESC = "PAKMVAdesc";
    public static final String DESC_VALUE = "Play Arts Kai by Square Enix";

    public String getDescValue(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        return settings.getString(DESC, DESC_VALUE);
    }

    public boolean updateDescValue(Activity activity, String descValue) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(DESC, descValue);

        return editor.commit();
    }

    public static final String IMAGEPATH = "PAKMVAimagepath";
    public static final String IMAGEPATH_VALUE = "playartskai.jpg";

    public String getImagepathValue(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        return settings.getString(IMAGEPATH, IMAGEPATH_VALUE);
    }

    public boolean updateImagepathValue(Activity activity, String imagepathValue) {
        SharedPreferences settings = activity.getSharedPreferences(PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(IMAGEPATH, imagepathValue);

        return editor.commit();
    }
}
