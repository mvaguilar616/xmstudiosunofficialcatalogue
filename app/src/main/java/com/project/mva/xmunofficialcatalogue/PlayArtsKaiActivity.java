package com.project.mva.xmunofficialcatalogue;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.mva.xmunofficialcatalogue.local.LocalPref;
import com.project.mva.xmunofficialcatalogue.models.FigureItem;
import com.project.mva.xmunofficialcatalogue.utils.InitJData;
import com.project.mva.xmunofficialcatalogue.utils.Utilities;

import org.json.JSONException;

import java.lang.reflect.Field;

public class PlayArtsKaiActivity extends Activity {

    private TextView name;
    private TextView price;
    private TextView releaseDate;
    private TextView desc;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        LocalPref storage = new LocalPref();

        String nameValue = storage.getNameValue(PlayArtsKaiActivity.this);

        try {
            FigureItem pakitem = fillDetails(nameValue);

            name = (TextView) findViewById(R.id.nameTextView);
            price = (TextView) findViewById(R.id.priceTextView);
            desc = (TextView) findViewById(R.id.descriptionValueTextView);
            image = (ImageView) findViewById(R.id.mainImageView);

            if (pakitem.getName().equalsIgnoreCase(nameValue)) {
                name.setText(pakitem.getName());
                price.setText("Retail Price: "+pakitem.getOgPrice());
                desc.setText(pakitem.getDescription());
                image.setImageResource(Utilities.getImgId(pakitem.getImagePath()));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private FigureItem fillDetails(String name) throws JSONException {
        InitJData jData = new InitJData();

        return Utilities.getPak(jData.getInitData(), name);
    }

    public static int getResId(String resName, Class<?> c) {

        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(PlayArtsKaiActivity.this, UniverseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
