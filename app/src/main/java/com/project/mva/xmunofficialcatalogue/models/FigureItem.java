package com.project.mva.xmunofficialcatalogue.models;

/**
 * Created by martinInExt on 25/04/2017.
 */

public class FigureItem {

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String universe;

    public String getUniverse() {
        return universe;
    }

    public void setUniverse(String universe) {
        this.universe = universe;
    }

    String ogPrice;

    public String getOgPrice() {
        return ogPrice;
    }

    public void setOgPrice(String ogPrice) {
        this.ogPrice = ogPrice;
    }

    String releaseDate;

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    String imagePath;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
