package com.project.mva.xmunofficialcatalogue;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.project.mva.xmunofficialcatalogue.local.LocalPref;
import com.project.mva.xmunofficialcatalogue.utils.InitJData;
import com.project.mva.xmunofficialcatalogue.utils.Utilities;

import org.json.JSONException;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
	//Main
            InitJData uniJdata = new InitJData();
            String[] universes = Utilities.getUniverses(uniJdata.getInitUniverse());

            ArrayAdapter adapter = new ArrayAdapter<String>(this,
                    R.layout.universe_listview, universes);


            final ListView listView = (ListView) findViewById(R.id.list);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // ListView Clicked item index
                    int itemPosition     = position;

                    // ListView Clicked item value
                    String  itemValue    = (String) listView.getItemAtPosition(position);

                    // Show Alert
    //                Toast.makeText(getApplicationContext(),
    //                        "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
    //                        .show();

                    LocalPref localStorage = new LocalPref();
                    localStorage.updateActiveUniverseValue(MainActivity.this, itemValue);

                    Intent intent = new Intent(MainActivity.this, UniverseActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
