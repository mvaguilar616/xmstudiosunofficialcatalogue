package com.project.mva.xmunofficialcatalogue;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.project.mva.xmunofficialcatalogue.local.LocalPref;
import com.project.mva.xmunofficialcatalogue.utils.InitJData;
import com.project.mva.xmunofficialcatalogue.utils.Utilities;

import org.json.JSONException;

public class UniverseActivity extends Activity {

    private TextView header;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_universe);

        try {
            LocalPref localStorage = new LocalPref();

            InitJData uniJdata = new InitJData();
            String[] paks = Utilities.getPaks(uniJdata.getInitData(), localStorage.getActiveUniverseValue(UniverseActivity.this));

            ArrayAdapter adapter = new ArrayAdapter<String>(this,
                    R.layout.pak_listview, paks);

            final ListView listView = (ListView) findViewById(R.id.paklist);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // ListView Clicked item index
                    int itemPosition     = position;

                    // ListView Clicked item value
                    String  itemValue    = (String) listView.getItemAtPosition(position);

                    // Show Alert
                    //                Toast.makeText(getApplicationContext(),
                    //                        "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
                    //                        .show();

                    LocalPref localStorage = new LocalPref();
                    localStorage.updateNameValue(UniverseActivity.this,itemValue);

                    Intent intent = new Intent(UniverseActivity.this, PlayArtsKaiActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        LocalPref storage = new LocalPref();

        header = (TextView) findViewById(R.id.universeTextView);
        header.setText(storage.getActiveUniverseValue(UniverseActivity.this));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UniverseActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
