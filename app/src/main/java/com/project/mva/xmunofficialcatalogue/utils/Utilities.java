package com.project.mva.xmunofficialcatalogue.utils;

import com.project.mva.xmunofficialcatalogue.R;
import com.project.mva.xmunofficialcatalogue.models.FigureItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by martinInExt on 25/04/2017.
 */

public class Utilities {

    public static String[] getUniverses(String jData) throws JSONException{

        JSONObject jsonObj = new JSONObject(jData);
        JSONArray universesJArray = jsonObj.getJSONArray("universes");
        int universesLength = universesJArray.length();
        String[] universes = new String[universesLength];

        for (int i = 0; i < universesLength; i++) {
            JSONObject jdata = universesJArray.getJSONObject(i);
            String universe = jdata.getString("name");

            universes[i] = universe;
        }

        return universes;
    }

    public static String[] getPaks(String jData, String universe) throws JSONException, NullPointerException{

        JSONObject jsonObj = new JSONObject(jData);
        JSONArray paksJArray = jsonObj.getJSONArray("figure");
        int paksLength = paksJArray.length();
        ArrayList<String> paks = new ArrayList<String>();

        for (int i = 0; i < paksLength; i++) {
            JSONObject jdata = paksJArray.getJSONObject(i);
            if (jdata.getString("universe").equalsIgnoreCase(universe)) {
                String pak = jdata.getString("name");
                paks.add(pak);
            }
        }

        String[] resultPaks = new String[paks.size()];
        resultPaks = paks.toArray(resultPaks);

        return resultPaks;
    }

    public static FigureItem getPak(String jData, String name) throws JSONException{

        JSONObject jsonObj = new JSONObject(jData);
        JSONArray paksJArray = jsonObj.getJSONArray("figure");
        int paksLength = paksJArray.length();
        FigureItem choice = new FigureItem();

        for (int i = 0; i < paksLength; i++) {
            JSONObject jdata = paksJArray.getJSONObject(i);
            FigureItem pakitem = new FigureItem();
            pakitem.setName(jdata.getString("name"));
            if (pakitem.getName().equals(name)) {
                pakitem.setOgPrice(jdata.getString("ogprice"));
                pakitem.setReleaseDate(jdata.getString("release date"));
                pakitem.setUniverse(jdata.getString("universe"));
                pakitem.setDescription(jdata.getString("description"));
                pakitem.setImagePath(jdata.getString("imagepath"));
                choice = pakitem;
            }
        }

        return choice;
    }

    public static int getImgId(String name) {
        int id = 0;

        if (name.equals("antman")) {
            id = R.drawable.antman;
        } else if (name.equals("blackwidow")) {
            id = R.drawable.blackwidow;
        } else if (name.equals("captam")) {
            id = R.drawable.captam;
        } else if (name.equals("hawkeye")) {
            id = R.drawable.hawkeye;
        } else if (name.equals("hulk")) {
            id = R.drawable.hulk;
        } else if (name.equals("ironman42")) {
            id = R.drawable.ironman42;
        } else if (name.equals("jeangrey")) {
            id = R.drawable.pheonix;
        } else if (name.equals("magneto")) {
            id = R.drawable.magneto;
        } else if (name.equals("wolverine")) {
            id = R.drawable.wolverine;
        } else if (name.equals("msmarvel")) {
            id = R.drawable.msmarvel;
        } else if (name.equals("ironman7")) {
            id = R.drawable.ironman7;
        } else if (name.equals("thanos")) {
            id = R.drawable.thanos;
        } else if (name.equals("batman")) {
            id = R.drawable.batman;
        } else {
            id = 0;
        }

        return id;
    }

}
