package com.project.mva.xmunofficialcatalogue.models;

/**
 * Created by martinInExt on 25/04/2017.
 */

public class Figures {

    String universe;

    public String getUniverse() {
        return universe;
    }

    public void setUniverse(String universe) {
        this.universe = universe;
    }

    FigureItem[] paksItems;

    public FigureItem[] getPaksItems() {
        return paksItems;
    }

    public void setPaksItems(FigureItem[] paksItems) {
        this.paksItems = paksItems;
    }

    public String[] pakNames() {
        int length = paksItems.length;

        String[] names = new String[length];

        for (int i = 0; i < length; i++) {
            names[i] = paksItems[i].getName();
        }

        return names;
    }

    public FigureItem getPakItem(String name) {

        int length = paksItems.length;
        FigureItem result = new FigureItem();

        for (int i = 0; i < length; i++) {

            if(paksItems[i].getName().equalsIgnoreCase(name)) {
                result.setName(paksItems[i].getName());
                result.setDescription(paksItems[i].getDescription());
                result.setUniverse(paksItems[i].getUniverse());
                result.setOgPrice(paksItems[i].getOgPrice());
                result.setReleaseDate(paksItems[i].getReleaseDate());
                result.setImagePath(paksItems[i].getImagePath());
            }

        }

        return result;
    }
}
